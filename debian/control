Source: frogr
Section: graphics
Priority: optional
Maintainer: Alberto Garcia <berto@igalia.com>
Build-Depends: debhelper (>= 10.3),
               meson,
               libgtk-3-dev (>= 3.4),
               libglib2.0-dev (>= 2.44),
               libgstreamer1.0-dev,
               libjson-glib-dev (>= 0.12),
               libsoup2.4-dev (>= 2.34),
               libxml2-dev (>= 2.6.8),
               libexif-dev (>= 0.6.14),
               yelp-tools,
               libgcrypt20-dev
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://wiki.gnome.org/Apps/Frogr
Vcs-Browser: https://salsa.debian.org/berto/frogr
Vcs-Git: https://salsa.debian.org/berto/frogr.git

Package: frogr
Architecture: any
Depends: frogr-data (= ${source:Version}),
         shared-mime-info,
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: gstreamer1.0-plugins-base,
            gstreamer1.0-plugins-good,
            yelp
Suggests: gstreamer1.0-plugins-bad,
          gstreamer1.0-plugins-ugly
Description: Flickr Remote Organizer for GNOME
 Frogr is a small application for the GNOME desktop that allows users
 to manage their accounts in the Flickr image hosting website.
 It supports all the basic tasks, including uploading pictures, adding
 descriptions, setting tags and managing sets.

Package: frogr-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: frogr (<< 0.7)
Replaces: frogr (<< 0.7)
Recommends: frogr
Description: Flickr Remote Organizer for GNOME - data files
 Frogr is a small application for the GNOME desktop that allows users
 to manage their accounts in the Flickr image hosting website.
 It supports all the basic tasks, including uploading pictures, adding
 descriptions, setting tags and managing sets.
 .
 This package contains the architecture-independent data files.
